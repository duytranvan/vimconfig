set nocompatible   
" Initialisation de pathogen
call pathogen#infect()
call pathogen#helptags()
if has("multi_byte")
  if &termencoding == ""
    let &termencoding = &encoding
  endif
  set encoding=utf-8
  setglobal fileencoding=utf-8
  "setglobal bomb
  "set fileencodings=ucs-bom,utf-8,lendif
endif
set number
filetype plugin indent on
syntax on
